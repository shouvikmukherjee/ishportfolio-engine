class IshportfolioCategoriesItems < ActiveRecord::Migration
  def change
  	create_table :ishportfolio_categories_items, :id => false do |t|
      t.integer :category_id
      t.integer :item_id
    end
  end
end
