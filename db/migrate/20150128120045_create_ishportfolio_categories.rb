class CreateIshportfolioCategories < ActiveRecord::Migration
  def change
    create_table :ishportfolio_categories do |t|
      t.string :title
      t.string :slug
      t.text :content
      t.string :image

      t.timestamps null: false
    end
    add_index :ishportfolio_categories, :slug
  end
end
