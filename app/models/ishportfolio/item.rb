module Ishportfolio
  class Item < ActiveRecord::Base
  	# Association
  	has_and_belongs_to_many :categories

  	# Uploader
	mount_uploader :image, ImageUploader

  	# Validation
  	validates :title, presence: true,
  	            length: { maximum: 250 }
  end
end
