module Ishportfolio
  class Category < ActiveRecord::Base
  	# This assumes you already have friendly_id in your gemfile and all that good stuff.
  	extend FriendlyId
  	friendly_id :slug, use: :slugged

    # Uploader
    mount_uploader :image, ImageUploader

  	# Association
  	has_and_belongs_to_many :items

  	# Fields validation
  	SLUG_REGEX = /\A^[\w&-]+$\Z/

  	validates :title, presence: true,
  	            length: { maximum: 250 }

  	validates :slug, presence: true,
  	            length: { maximum: 250 },
  	            format: SLUG_REGEX
  end
end
