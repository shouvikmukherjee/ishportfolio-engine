module Ishportfolio
  module ApplicationHelper
  	# Generates entity cover image
  	def load_ishportfolio_image(entity, size={})
  		size = size ||= :medium
  		if !entity.image_url.blank?
  			image_tag( entity.image_url(size).to_s, { :class => 'img-responsive', :alt => entity.title, :title => entity.title })
  		end
  	end

  	# Generates entity excerpt
	def load_ishportfolio_excerpt(entity, length=100)
		truncate(strip_tags(entity.content).html_safe, :length => length, :escape => false)
	end
  end
end
