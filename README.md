# iShPortfolo Rails Engine
This is a simple ROR Engine for dynamically managing portfolio on ROR apps.

## Dependencies
-	Devise: https://github.com/plataformatec/devise
-	Cancan: https://github.com/ryanb/cancan
-	Friendly Id: https://github.com/norman/friendly_id
-	Kaminary: https://github.com/amatsuda/kaminari

## Installation
-	Install the gem
	```
	gem 'ishportfolio', :git => 'git@bitbucket.org:shouvikmukherjee/ishportfolio-engine.git'
	```

-	Copy Migrations
	```
	rake ishportfolio:install:migrations
	```

-	Run Migrations
	```
	rake db:migrate
	```

-	Mount the Routes config
	```
	mount Ishportfolio::Engine => "/portfolio"
	```

-	Require css and js assets
	```
	*= require ishportfolio/application
	```	

	```
	//= require ishportfolio/application
	```