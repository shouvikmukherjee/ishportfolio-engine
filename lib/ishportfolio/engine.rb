module Ishportfolio
  class Engine < ::Rails::Engine
    isolate_namespace Ishportfolio
  end

  require 'cancan'
  require 'friendly_id'
  require 'carrierwave'
  require 'rmagick'
  require 'kaminari'
end
