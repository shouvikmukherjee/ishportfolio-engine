$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ishportfolio/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ishportfolio"
  s.version     = Ishportfolio::VERSION
  s.authors     = ["Shouvik Mukherjee"]
  s.email       = ["contact@ishouvik.com"]
  s.homepage    = "TODO"
  s.summary     = "TODO: Summary of Ishportfolio."
  s.description = "TODO: Description of Ishportfolio."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0"

  s.add_dependency "cancan"
  s.add_dependency "friendly_id"
  s.add_dependency "carrierwave"
  s.add_dependency "rmagick"
  s.add_dependency "sass-rails"
  s.add_dependency "kaminari"
  s.add_dependency "fog"

  s.add_development_dependency "sqlite3"
end
