Ishportfolio::Engine.routes.draw do
  resources :categories
  resources :items
  get '/', :to => 'items#index'
end
